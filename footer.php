
        <div class="footer-container">
            <footer class="wrapper clearfix">
                <div class="footer-left">
                  <div id="footer-nav">
                      <ul>
                          <li><a href="/our-firm/contact-us/#atlanta">Atlanta</a><span>|</span></li>
                          <li><a href="/our-firm/contact-us/#greensboro">Greensboro</a><span>|</span></li>
                          <li><a href="/our-firm/contact-us/#memphis">Memphis</a><span>|</span></li>
                          <li><a href="/our-firm/contact-us/#nashville">Nashville</a></li>
                      </ul>
                  </div>
                </div>
                <div class="footer-middle">
                  <div id="social-links">
                    Find Us On
                    <div id="social-links-wrapper" class="clearfix">
                      <a href="https://www.linkedin.com/company/diversified-trust" id="social-link-linkedin" target="_blank">LinkedIn</a>
                      <a href="https://twitter.com/DiversifiedCWM" id="social-link-twitter" target="_blank">Twitter</a>
                    </div>
                  </div>
                </div>
                <div class="footer-right">
                  <div id="disclosure">©2007-2016 Diversified Trust | <a href="/disclosure">Disclosure</a> | <a href="/privacy-policy">Privacy Policy</a></div>
                </div>
            </footer>
        </div>
        <!-- SCRIPTS -->
                        <script>
        jQuery(document).ready(function($) {        
            // HIDE EXPAND FILTER OPTIONS
            // NEWS PDF HIDE/SHOW
            $(".pdf-downloads .news-button").click(function() {
                $(this).next().slideToggle(500);
                $(this).toggleClass('open');
                $(this).find('span').toggleClass('pdfs-open');
            });

            // OUR MENU
            $('#main-nav > ul#mobile-menu > li ul').each(function(index, e){
              var count = $(e).find('li').length;
            });
            $('#main-nav ul#mobile-menu ul li:odd').addClass('odd');
            $('#main-nav ul#mobile-menu ul li:even').addClass('even');
            $('#mobile-trigger').click(function() {
              $('#mobile-menu').toggle();
            });
            $('#main-nav > ul#mobile-menu > li > a').click(function() {
              $('#main-nav li').removeClass('active');
              $(this).closest('li').addClass('active'); 
              var checkElement = $(this).next();
              if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('active');
                checkElement.slideUp('normal');
              }
              if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#main-nav ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
              }
              if($(this).closest('li').find('ul').children().length == 0) {
                return true;
              } else {
                return false;   
              }     
            });
                     });
        </script>
        
        <script>         
         jQuery(document).ready(function() {
          	jQuery('.videoThumb').click(function(){
							var video = jQuery(this);
							var videoID = video.attr('rel');
							
							jQuery(".videoMain .inner").fadeOut(50, function() {
					  		jQuery(this).empty().append('<iframe width="500" height="281" src="http://www.youtube.com/embed/' + videoID + '?rel=0;autohide=1;showinfo=0;?modestbranding=0;iv_load_policy=3;autoplay=1" frameborder="0" allowfullscreen></iframe>').fadeIn(50);
					  	});
					  	
					  	video.addClass('selected').siblings().removeClass('selected');
							
          	});
         });
         
         jQuery('.featuredImage.video').click(function(){
	         vidID = jQuery(this).attr('rel');
	         jQuery(this).addClass('active');
	         jQuery(this).append('<iframe width="500" height="281" src="http://www.youtube.com/embed/' + vidID + '?rel=0;autohide=1;showinfo=0;?modestbranding=0;iv_load_policy=3;autoplay=1" frameborder="0" allowfullscreen></iframe>').fadeIn(10);
         });
         
         
         jQuery('.article.post-single').find('iframe[src*="youtube"]').each(function(){
	         var src = jQuery(this).attr('src');
	         jQuery(this).attr('src', src + '&rel=0;showinfo=0;modestbranding=0');
         });
       	</script>
        
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-39765205-21', 'auto');
          ga('send', 'pageview');

        </script>
  <script type="text/javascript" src="http://www.diversifiedtrust.com/wp-includes/js/admin-bar.min.js"></script>
<script type="text/javascript" src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/library/js/scripts.js"></script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
			
		
<script type='text/javascript'>
var e_mailit_config = {"display_counter":false,"after_share_dialog":true,"mobile_bar":false,"hover_pinit":false,"display_ads":true,"open_on":"onclick","emailit_branding":true,"mobile_back_color":"","thanks_message":"Thanks for sharing!","follow_services":{},"mobileServices":"Facebook,Twitter,LinkedIn,Send_via_Email,EMAILiT"};
(function() {var b=document.createElement('script');b.type='text/javascript';b.async=true;b.src='//e-mailit.com/widget/menu3x/js/button.js';var c=document.getElementsByTagName('head')[0];c.appendChild(b) })()
</script>
		      


<script id="hiddenlpsubmitdiv" style="display: none;"></script><script>try{(function() { for(var lastpass_iter=0; lastpass_iter < document.forms.length; lastpass_iter++){ var lastpass_f = document.forms[lastpass_iter]; if(typeof(lastpass_f.lpsubmitorig2)=="undefined"){ lastpass_f.lpsubmitorig2 = lastpass_f.submit; if (typeof(lastpass_f.lpsubmitorig2)=='object'){ continue;}lastpass_f.submit = function(){ var form=this; var customEvent = document.createEvent("Event"); customEvent.initEvent("lpCustomEvent", true, true); var d = document.getElementById("hiddenlpsubmitdiv"); if (d) {for(var i = 0; i < document.forms.length; i++){ if(document.forms[i]==form){ if (typeof(d.innerText) != 'undefined') { d.innerText=i.toString(); } else { d.textContent=i.toString(); } } } d.dispatchEvent(customEvent); }form.lpsubmitorig2(); } } }})()}catch(e){}</script></body></html>