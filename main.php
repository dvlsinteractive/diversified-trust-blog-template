<div id="banner-container" class="blog archive">
	<div class="center">
		<div class="intro">
			<h1>The Big Picture</h1>
			<p>Diversified Trust experts share their knowledge and insights on wealth strategies, investing and current financial news and trends.</p>
		<div class="group"></div>
		</div>
		
		<div class="categories">
			<a href="#">
				<?php include('includes/icon-wealth_strategies.php'); ?>
				<span>Wealth Strategies</span>
			</a>
			
			<a href="#">
				<?php include('includes/icon-investing.php'); ?>
				<span>Investing</span>
			</a>
			
			<a href="#">
				<?php include('includes/icon-insights.php'); ?>
				<span>Insights</span>
			</a>
		</div>
		
		<div class="all">
			<a href="#">Show All Articles</a>
		</div>
	</div>
</div> <!--end banner-container-->

<div class="main-container">
	<div class="wrapper clearfix">
	  <div class="posts">
		  <div class="post-item">
			  <div class="image video" style="background:url('http://img.youtube.com/vi/9fhjAauhV74/maxresdefault.jpg');">
				  <a href="#"></a>
			  </div>
			  
			  <div class="text">
			  	<h2><a href="#">Checking Market Expectations and Looking Forward</a></h2>
					<p class="subtext">February 19th, 2016 | by Andrew Berg</p>
			  
				  <span class="excerpt">
				  	<p>The S&P 500 is once again the investing world’s favored index due in part to a cumulative +217% return between the Great Recession’s low point in March 2009 and the end of calendar 2014, and a staggering annualized rate of +21.9% per year. It also serves as the proxy by which many institutional and individual...</p>

				  	<a href="#" class="readMore">Read More</a>
				  </span>
				  
				  <div class="group"></div>
			  </div><!-- .text -->
		  </div><!-- .post-item -->
		  
		  
		  <div class="post-item">
			  <div class="image" style="background-image:url('webimages/temp.jpg');">
				  <a href="#"></a>
			  </div>
			  
			  <div class="text">
			  	<h2><a href="#">Venture “Unicorns”</a></h2>
					<p class="subtext">February 19th, 2006 | by <a href="#">Lamar Stanley</a></p>
			  
				  <span class="excerpt">
				  	<p>There has been extensive media coverage and discussion over the last 18-24 months about the increasing number of venture backed private companies valued at over $1 billion, often referred to as “unicorns.” The Wall Street Journal keeps tabs on these unicorns with their “Billion Dollar Startup Club”, and it seems whenever you open a business...</p>
				  	
				  	<a href="#" class="readMore">Read More</a>
				  </span>
				  
				  <div class="group"></div>
			  </div><!-- .text -->
		  </div><!-- .post-item -->
		  
		  
		  <div class="post-item">
			  <div class="image" style="background-image:url('webimages/temp.jpg');">
				  <a href="#"></a>
			  </div>
			  
			  <div class="text">
			  	<h2><a href="#">The Millennial Investing Advantage: Time</a></h2>
					<p class="subtext">February 16th, 2016 | by <a href="#">Adam Dretler</a></p>
			  
				  <span class="excerpt">
				  	<p>One of the greatest competitive advantages in investing is a long time horizon.    A phrase I have often used to remind people of the importance of looking at the long-term is the following:  “pull back the chart.”  This simply means that we should look at investment performance from a longer time horizon rather than...</p>
				  	
				  	<a href="#" class="readMore">Read More</a>
				  </span>
				  
				  <div class="group"></div>
			  </div><!-- .text -->
		  </div><!-- .post-item -->
		  
		  		  
	  </div><!--.posts-->		    
	  <div class="clearfix"></div>
	  
		
		
		<div class="pagination">
			<span>Page 2 of 5</span>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/" class="pagePrev">‹</a>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/" class="inactive">1</a>
			<span class="current">2</span>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/page/3/" class="inactive">3</a>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/page/4/" class="inactive">4</a>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/page/5/" class="inactive">5</a>
			<a href="http://www.diversifiedtrust.com/news-resources/the-big-picture/page/3/" class="pageNext">›</a>
		</div>
		
		
	</div> <!-- .wrapper -->
</div> <!-- #main-container -->

