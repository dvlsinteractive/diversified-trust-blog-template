<div class="printLogo">
	
	<div class="border">
		<img src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/includes/img/logo-square.jpg" />
	</div>
</div>

<div id="banner-container" class="blog single">
	<div class="center">
		<h1>The Big Picture</h1>
		<div class="group"></div>
			<a href="#">back to list</a>
		
	</div>
</div> <!--end banner-container-->


<div class="main-container">
          <div class="wrapper clearfix">
	          
	          	
	          	
      <div class="article post-single">
       
       <div class="featuredImage" style="background-image:url('library/images/temp.jpg');"></div>
       
<!-- 			<div class="featuredImage video" style="background:url('http://img.youtube.com/vi/9fhjAauhV74/maxresdefault.jpg');" rel="9fhjAauhV74"></div>                 -->
                      
			 <h1>Risk? It Depends on Your Point of View It Depends on Your Point of View</h1>
			 <p class="date">February 8th, 2016 | posted in: <a href="http://www.diversifiedtrust.com/blog/investing/" rel="category tag">Investing</a></p>
			 <p class="printAuthor">
								 by: 
								 									Bradley Crawford									&amp; 										
																		T. Vincent Chamblee																			
																 </p>
			
			 
			 <div class="text">
				
				
				
				<form method="GET"	action="mailto:<?php echo $_GET['Email'];?>?subject=testing">
					<input type="text" name="email" id="email" />
					<input type="submit" value="Submit">
				</form>
				
			
				 
				 <p>Investment risk is a challenging topic because market participants measure and define it so differently.&nbsp; For some, it is a quantifiable exercise but for others it can&nbsp;be much more subjective.&nbsp;&nbsp;The white paper,&nbsp;“<a href="http://www.diversifiedtrust.com/wp-content/uploads/2014/11/2014-Fourth-Quarter-Risk-The-Most-Difficult-Topic-in-Investing-Bill-Spitz1.pdf">Risk – The Most Difficult Topic in Investing</a>” examines the four commonly used definitions for investment risk: chance of losing money, permanent loss of capital, probability of not achieving one’s goals and volatility of return.</p>
				 
<p>Understanding one’s long term return objective as well as one’s tolerance for short term volatility is crucial in order to determine an appropriate asset allocation.&nbsp; Wealth managers who ask these questions are better equipped to design portfolios that succeed in meeting a client’s goals.</p>
<p>Read more on this topic by clicking <a href="http://www.diversifiedtrust.com/wp-content/uploads/2014/11/2014-Fourth-Quarter-Risk-The-Most-Difficult-Topic-in-Investing-Bill-Spitz1.pdf">here.</a></p>
			 </div>
			 
			 <div class="tags">
				 <h3>Tags:</h3><a href="http://www.diversifiedtrust.com/tag/white-paper/" rel="tag">white paper</a><br> 
			 </div>
			 
       



       
       								
      </div><!-- end post-single -->
      
              
              
						 <div class="blogSidebar">
		          	<div class="authorInfo">
			          	<div class="postAuthor">

			          											<span class="thumbnail">
											<img src="http://www.diversifiedtrust.com/wp-content/uploads/2014/09/20140223-DTrust-Memphis1-hs-0425-Edit.jpg">
										</span>
										<span class="clearfix"></span>
										
																				
										<span class="contactInfo">
											<p class="name">Bradley Crawford</p>
											<a class="bio" href="http://www.diversifiedtrust.com/people/memphis/bradley-crawford-cfp">view bio</a>
											
											
											

																							<br>
											
										</span>
			          	</div>
			          	
			          	<div class="postAuthor">
										
										
																			<span class="thumbnail">
											<img src="http://www.diversifiedtrust.com/wp-content/uploads/2014/09/20140223-DTrust-Memphis1-hs-0461-Edit.jpg">
										</span>
										<span class="clearfix"></span>
										
																				
										<span class="contactInfo">
											<p class="name">T. Vincent Chamblee</p>
											<a class="bio" href="http://www.diversifiedtrust.com/people/memphis/t-vincent-chamblee-2">view bio</a>
											
											
											<a class="email" href="mailto:vchamblee@diversifiedtrust.com" data-email="vchamblee@diversifiedtrust.com">email T. Vincent</a>
											

											
										</span>
			          	</div>
										
										
										          	</div>
		          	<br>
		          	<h2 class="share">Share This Article:</h2>
		          	<div class="clearfix"></div>	
		          	<div class="e-mailit_toolbox large ">
								  <div class="e-mailit_btn_Facebook"></div>
								  <div class="e-mailit_btn_Twitter"></div>
								  <div class="e-mailit_btn_LinkedIn"></div>
								  <div class="e-mailit_btn_Send_via_Email"></div>
								  <a href="javascript:window.print()" class="printBtn"><img src="library/images/print.svg" /></a>
								</div>
								
		          	
		          	<div class="clearfix"></div>
		          	
		          	
		          	<div class="related">
								
																	
									<h2>More posts by William:</h2>
									
									<ul>
																														
																				
										<li><a href="http://www.diversifiedtrust.com/managing-volatility-in-portfolios/">The Power of Lower Volatility</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/the-importance-of-examining-investment-returns/">The Importance of Examining Investment Returns</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/avoiding-financial-behavioral-mistakes/">Behavioral Finance: An Overview</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/how-to-avoid-mistakes-when-investing/">Top Ten Most Common Investing Mistakes</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/how-to-think-about-risk-when-investing/">Risk? It Depends on Your Point of View</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/diversification/">The Importance of Diversification</a></li>
										
																			</ul>
									
																 </div>
								
	          </div>
              
             <div class="clearfix"></div>

          </div> <!-- #main -->
      </div>