// load Gulp dependencies
var gulp = require('gulp'); 
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var cssbeautify = require('gulp-cssbeautify');
var autoprefixer = require('gulp-autoprefixer');
var cmq = require('gulp-combine-media-queries');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');

	// ftp upload settings
	var host = 'diversifiedtrust.com';
	var user = 'dtc';
	var password = '3Ztx)ip6obJb';
	var localFilesGlob = ['library/css/blog.css'];  
	var remoteFolder = 'public_html/wp-content/themes/diversified-trust/'



function onError(err) {
    console.log(err);
}


// watch SCSS
gulp.task('styles', function () {

  // compile scss
	return sass('sass/blog.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    
    // automatically add needed browser prefixing
    .pipe(autoprefixer({
        browsers: ['> 5%', 'IE 9','last 2 versions'],
        cascade: false,
        remove: true
    }))
    
    // combine media queries/move to end of document
    .pipe(cmq({
      log: true
    }))
    
    .pipe(cssbeautify({
	    indent: '	',
	    openbrace: 'end-of-line',
	    autosemicolon: true
    }))
	  
	  // rename in folder
    .pipe(gulp.dest('library/css'))
    
    // re-inject styles into page
    .pipe(livereload());
});





// watch files for changes
gulp.task('watch', function() {	
	livereload.listen();

  //watch and reload PHP and html
  gulp.watch('**/*.php').on('change', function(file) {
  	livereload.changed(file.path); 
  });
  
  gulp.watch('**/*.html').on('change', function(file) {
  	livereload.changed(file.path);
 	});

  gulp.watch('sass/*.scss', ['styles']);
});



// deploy via ftp 
gulp.task('deploy', function() {

    var connection = ftp.create( {
      host: host,
      user: user,
      password: password,
      parallel: 10,
      log: gutil.log
  });

    gulp.watch(localFilesGlob)
    .on('change', function(event) {
      console.log('Uploading file "' + event.path + '", ' + event.type);

    return gulp.src(localFilesGlob, { base: '.', buffer: false })
      .pipe(connection.newer(remoteFolder)) // only upload newer files 
      .pipe(connection.dest(remoteFolder));
  });
});





// Ready? Set... Go!
gulp.task('default', ['styles', 'watch', 'deploy']);	

// (for no FTP upload - local only - run "gulp local")
gulp.task('local', ['styles', 'watch']);	
