<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports"><!--<![endif]--><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>The Big Picture - Diversified Trust</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/includes/css/normalize.min.css">
        <link href="http://fonts.googleapis.com/css?family=Cinzel:400,700,900" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/includes/css/main.css?v=1.7">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <script async="" src="//www.google-analytics.com/analytics.js"></script><script src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/includes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- INSERT MENU STYLES -->
        
<!-- This site is optimized with the Yoast SEO plugin v2.3.4 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="noindex,follow">
<!-- Admin only notice: this page doesn't show a meta description because it doesn't have one, either write it for this page specifically or go into the SEO -> Titles menu and set up a template. -->
<link rel="canonical" href="http://www.diversifiedtrust.com/news-resources/the-big-picture/">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="article">
<meta property="og:title" content="The Big Picture - Diversified Trust">
<meta property="og:url" content="http://www.diversifiedtrust.com/news-resources/the-big-picture/">
<meta property="og:site_name" content="Diversified Trust">
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Diversified Trust » Feed" href="http://www.diversifiedtrust.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Diversified Trust » Comments Feed" href="http://www.diversifiedtrust.com/comments/feed/">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.diversifiedtrust.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><script src="http://www.diversifiedtrust.com/wp-includes/js/wp-emoji-release.min.js" type="text/javascript"></script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="open-sans-css" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext" type="text/css" media="all">
<link rel="stylesheet" id="dashicons-css" href="http://www.diversifiedtrust.com/wp-includes/css/dashicons.min.css" type="text/css" media="all">
<link rel="stylesheet" id="admin-bar-css" href="http://www.diversifiedtrust.com/wp-includes/css/admin-bar.min.css" type="text/css" media="all">
<link rel="stylesheet" id="boxes-css" href="http://www.diversifiedtrust.com/wp-content/plugins/wordpress-seo/css/adminbar.min.css" type="text/css" media="all">
<link rel="stylesheet" id="bones-stylesheet-css" href="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/library/css/style.css" type="text/css" media="all">
<!--[if lt IE 9]>
<link rel='stylesheet' id='bones-ie-only-css'  href='http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/library/css/ie.css' type='text/css' media='all' />
<![endif]-->
<link rel="stylesheet" id="mpce-theme-css" href="http://www.diversifiedtrust.com/wp-content/plugins/motopress-content-editor/includes/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" id="mpce-bootstrap-grid-css" href="http://www.diversifiedtrust.com/wp-content/plugins/motopress-content-editor/bootstrap/bootstrap-grid.min.css" type="text/css" media="all">
<link rel="stylesheet" id="mpce-font-awesome-css" href="http://www.diversifiedtrust.com/wp-content/plugins/motopress-content-editor/fonts/font-awesome/css/font-awesome.min.css" type="text/css" media="all">
<link rel="stylesheet" id="googleFonts-css" href="http://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C400italic%2C700italic" type="text/css" media="all">
<script type="text/javascript" src="http://www.diversifiedtrust.com/wp-includes/js/jquery/jquery.js"></script>
<script type="text/javascript" src="http://www.diversifiedtrust.com/wp-includes/js/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/mp-custom-google-charts.js"></script>
<script type="text/javascript" src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/library/js/libs/modernizr.custom.min.js"></script>
<link rel="shortlink" href="http://www.diversifiedtrust.com/?p=1638">
<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>




<!-- additional blog styles -->
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7688694/6303152/css/fonts.css" />
<link rel='stylesheet' href="library/css/blog.css">

<script src="http://localhost:35729/livereload.js?snipver=1"></script>
<style>
	body:before {
		background:#c0392b;
		display: block;
		color:#fff;
		padding:2%;
	}
</style>






<!-- MotoPress Custom CSS Start -->
<style type="text/css">
@import url('http://www.diversifiedtrust.com/wp-content/uploads/motopress-content-editor/motopress-ce-custom.css?1439947452');
</style>
<!-- MotoPress Custom CSS End -->
    </head>
    <body class=" customize-support">

        <div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title"><a href="/">Diversified Trust</a></h1>
                <nav id="utility-nav">
                    <ul>
                        <li class="nav-client-login"><a href="https://clientpoint.fisglobal.com/tdcb/main/UserLogon?bankNumber=JI&amp;subProduct=DTC">Client login</a></li>
                        <li class="nav-contact-us"><a href="/our-firm/contact-us/">Contact us</a></li>
                        <li class="nav-search">
                            <form role="search" method="get" class="search-form" action="http://www.diversifiedtrust.com/">
                                <input placeholder="Search" value="" name="s" title="Search for:" type="search">
                            </form>
                        </li>
                    </ul>
                </nav>

                <nav id="main-nav">
                    <!-- FULL NAV -->
                    <ul id="menu">
                        <li><a href="#" class="pad-first pad-right">Our Firm</a>
                           <ul class="main-nav-sub">
                               <li class="page_item page-item-6"><a href="http://www.diversifiedtrust.com/our-firm/our-history/">Our History</a></li>
<li class="page_item page-item-13"><a href="http://www.diversifiedtrust.com/our-firm/mission-vision-values/">Mission, Vision &amp; Values</a></li>
<li class="page_item page-item-25"><a href="http://www.diversifiedtrust.com/our-firm/our-pledge/">Our Pledge</a></li>
<li class="page_item page-item-27"><a href="http://www.diversifiedtrust.com/our-firm/a-foundation-built-on-trust/">A Foundation Built on Trust</a></li>
<li class="page_item page-item-18"><a href="http://www.diversifiedtrust.com/our-firm/contact-us/">Contact us</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left pad-right">Our People</a>
                           <ul class="main-nav-sub">
                              <li class="cat-item cat-item-31"><a href="/our-people/board-of-directors/" title="1">Board of Directors</a></li>
                              <li class="cat-item cat-item-30"><a href="/our-people/corporate-team/">Corporate Team</a></li>
                              <li class="cat-item cat-item-29"><a href="/our-people/investment-team/">Investment Team</a></li>
                              <li class="cat-item cat-item-22"><a href="/our-people/atlanta/">Atlanta</a></li>
                              <li class="cat-item cat-item-23"><a href="/our-people/greensboro/">Greensboro</a></li>
                              <li class="cat-item cat-item-24"><a href="/our-people/memphis/">Memphis</a></li>
                              <li class="cat-item cat-item-28"><a href="/our-people/nashville/">Nashville</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left pad-right">Our Services</a>
                           <ul class="main-nav-sub">
                               <li class="page_item page-item-98"><a href="http://www.diversifiedtrust.com/our-services/investment-management/">Investment Management</a></li>
<li class="page_item page-item-100"><a href="http://www.diversifiedtrust.com/our-services/advisory-family-office/">Advisory &amp; Family Office</a></li>
<li class="page_item page-item-102"><a href="http://www.diversifiedtrust.com/our-services/trusts-and-estates/">Trusts &amp; Estates</a></li>
<li class="page_item page-item-104"><a href="http://www.diversifiedtrust.com/our-services/institutional-services/">Institutional Services</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left">News &amp; Resources</a>
                           <ul class="main-nav-sub">
                               <li class="page_item page-item-460"><a href="http://www.diversifiedtrust.com/news-resources/whitepapers/">White Papers</a></li>
<li class="page_item page-item-109"><a href="http://www.diversifiedtrust.com/news-resources/in-the-news/">In the News</a></li>
                           </ul>
                        </li>
                    </ul>
                    <!-- MOBILE NAV -->
                    <a id="mobile-trigger">MENU</a>
                    <ul id="mobile-menu">
                        <li><a href="#" class="pad-first pad-right">Our Firm</a>
                           <ul class="mobile-nav-sub">
                               <li class="page_item page-item-6 even"><a href="http://www.diversifiedtrust.com/our-firm/our-history/">Our History</a></li>
<li class="page_item page-item-13 odd"><a href="http://www.diversifiedtrust.com/our-firm/mission-vision-values/">Mission, Vision &amp; Values</a></li>
<li class="page_item page-item-25 even"><a href="http://www.diversifiedtrust.com/our-firm/our-pledge/">Our Pledge</a></li>
<li class="page_item page-item-27 odd"><a href="http://www.diversifiedtrust.com/our-firm/a-foundation-built-on-trust/">A Foundation Built on Trust</a></li>
<li class="page_item page-item-18 even"><a href="http://www.diversifiedtrust.com/our-firm/contact-us/">Contact us</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left pad-right">Our People</a>
                           <ul class="mobile-nav-sub">
                              <li class="cat-item cat-item-31 odd"><a href="/our-people/board-of-directors/" title="1">Board of Directors</a></li>
                              <li class="cat-item cat-item-30 even"><a href="/our-people/corporate-team/">Corporate Team</a></li>
                              <li class="cat-item cat-item-29 odd"><a href="/our-people/investment-team/">Investment Team</a></li>
                              <li class="cat-item cat-item-22 even"><a href="/our-people/atlanta/">Atlanta</a></li>
                              <li class="cat-item cat-item-23 odd"><a href="/our-people/greensboro/">Greensboro</a></li>
                              <li class="cat-item cat-item-24 even"><a href="/our-people/memphis/">Memphis</a></li>
                              <li class="cat-item cat-item-28 odd"><a href="/our-people/nashville/">Nashville</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left pad-right">Our Services</a>
                           <ul class="mobile-nav-sub">
                               <li class="page_item page-item-98 even"><a href="http://www.diversifiedtrust.com/our-services/investment-management/">Investment Management</a></li>
<li class="page_item page-item-100 odd"><a href="http://www.diversifiedtrust.com/our-services/advisory-family-office/">Advisory &amp; Family Office</a></li>
<li class="page_item page-item-102 even"><a href="http://www.diversifiedtrust.com/our-services/trusts-and-estates/">Trusts &amp; Estates</a></li>
<li class="page_item page-item-104 odd"><a href="http://www.diversifiedtrust.com/our-services/institutional-services/">Institutional Services</a></li>
                           </ul>
                        </li>
                        <li><a href="#" class="pad-left">News &amp; Resources</a>
                           <ul class="mobile-nav-sub">
                               <li class="page_item page-item-460 even"><a href="http://www.diversifiedtrust.com/news-resources/whitepapers/">White Papers</a></li>
<li class="page_item page-item-109 odd"><a href="http://www.diversifiedtrust.com/news-resources/in-the-news/">In the News</a></li>
                           </ul>
                        </li>
                        <li><a href="/our-firm/contact-us/" class="pad-left">Contact Us</a></li>
                    </ul>

                </nav>

            </header>
        </div>