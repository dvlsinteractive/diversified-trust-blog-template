<div class="printLogo">
	
	<div class="border">
		<img src="http://www.diversifiedtrust.com/wp-content/themes/diversified-trust/includes/img/logo-square.jpg" />
	</div>
</div>

<div id="banner-container" class="blog single">
	<div class="center">
		<h1>The Big Picture</h1>
		<div class="group"></div>
			<a href="#">back to list</a>
		
	</div>
</div> <!--end banner-container-->


<div class="main-container">
          <div class="wrapper clearfix">
	          
	          	
	          	
      <div class="article post-single">
              
                            
               <div class="featuredImage" style="background-image:url('http://www.diversifiedtrust.com/wp-content/uploads/2016/02/iStock_000059608952_Medium.jpg');"></div>
               
                             
							 <h1>Managing Volatility</h1>
							 <p class="date">March 5th, 2016 | posted in: <a href="http://www.diversifiedtrust.com/blog/investing/" rel="category tag">Investing</a></p>
							 
							 <p class="printAuthor">
								 by: 
								 									William Spitz																			
																 </p>
								 
							 
							
							 
							 <div class="text">
								 <p>Managing volatility allows a portfolio to provide more spending power over time.</p>
<p><iframe width="640" height="360" src="https://www.youtube.com/embed/wZF0SsloTMA?feature=oembed" frameborder="0" allowfullscreen=""></iframe></p>
							 </div>
							 
							 <div class="tags">
								  
							 </div>
               
               								
								
              </div>
      
              
              
						 <div class="blogSidebar">
		          	<div class="authorInfo">
			          	<div class="postAuthor">

			          											<span class="thumbnail">
											<img src="http://www.diversifiedtrust.com/wp-content/uploads/2014/09/20140223-DTrust-Memphis1-hs-0425-Edit.jpg">
										</span>
										<span class="clearfix"></span>
										
																				
										<span class="contactInfo">
											<p class="name">Bradley Crawford</p>
											<a class="bio" href="http://www.diversifiedtrust.com/people/memphis/bradley-crawford-cfp">view bio</a>
											
											
											

																							<br>
											
										</span>
			          	</div>
			          	
			          	<div class="postAuthor">
										
										
																			<span class="thumbnail">
											<img src="http://www.diversifiedtrust.com/wp-content/uploads/2014/09/20140223-DTrust-Memphis1-hs-0461-Edit.jpg">
										</span>
										<span class="clearfix"></span>
										
																				
										<span class="contactInfo">
											<p class="name">T. Vincent Chamblee</p>
											<a class="bio" href="http://www.diversifiedtrust.com/people/memphis/t-vincent-chamblee-2">view bio</a>
											
											
											<a class="email" href="mailto:vchamblee@diversifiedtrust.com" data-email="vchamblee@diversifiedtrust.com">email T. Vincent</a>
											

											
										</span>
			          	</div>
										
										
										          	</div>
		          	<br>
		          	<h2 class="share">Share This Article:</h2>
		          	
		          	<div class="clearfix"></div>
		          	
		          	
		          	<div class="related">
								
																	
									<h2>More posts by William:</h2>
									
									<ul>
																														
																				
										<li><a href="http://www.diversifiedtrust.com/managing-volatility-in-portfolios/">The Power of Lower Volatility</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/the-importance-of-examining-investment-returns/">The Importance of Examining Investment Returns</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/avoiding-financial-behavioral-mistakes/">Behavioral Finance: An Overview</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/how-to-avoid-mistakes-when-investing/">Top Ten Most Common Investing Mistakes</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/how-to-think-about-risk-when-investing/">Risk? It Depends on Your Point of View</a></li>
										
																				
										<li><a href="http://www.diversifiedtrust.com/diversification/">The Importance of Diversification</a></li>
										
																			</ul>
									
																 </div>
								
	          </div>
              
             <div class="clearfix"></div>

          </div> <!-- #main -->
      </div>